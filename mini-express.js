const http = require('http')
const url = require('url')
const querystring = require('querystring')

const routes = {
  GET:{},
  POST: {},
}

module.exports = function createServer() {
  const server = http.createServer((request, response) => {
    switch (request.method) {
      case 'GET':
      case 'POST':
        const {pathname, query} = url.parse(request.url)
        const handler = routes[request.method][pathname]

        if (query) {
          request.query = querystring.parse(query)
        }
        if (request.method === 'POST') {
          let data = ''
          request.on('data', (chunk) => {
            data+=chunk
          })
          request.on('end', ()=>{
            request.body = querystring.parse(data)
            handler(request, response)
          })
        } else if (request.method === 'GET' && handler) {
          handler(request, response)
        } else {
          response.write(
            `
            <html>
              <head>
                <title>Page Not Found</title>
              </head>
              <style>
                body{
                  background-image: linear-gradient(to right, black, darkcyan);
                  color: white;
                  text-align: center;
                }
              </style>
              <body>
                ERROR 404, Page Not Found!
              </body>
            </html>
            `
          )
          response.end() 
        }
        break
    default:
      break
    }
  })
  server.get = (url, handler) => {
    routes['GET'][url] = handler
  }
  server.post = (url, handler) => {
    routes['POST'][url] = handler
  }
  return server
}