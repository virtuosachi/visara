module.exports = function signUp (request, response) {
  response.writeHead(200, {'Content-Type': 'text/html'})
  response.write(
  `
  <html>
  <head>
    <title>Log in</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <style>
    .wrap {
      max-width: 300px;
      margin: auto;
      background-color: white; 
      padding: 30px;
      color: black;
      border-color: white;
      box-shadow: 0px 0px 10px grey;
    }
    input[type="text"], input[type="password"] {
      width: 300px;
      font-family: 'sans-serif';
      margin-bottom: 2px;
      margin-top: 25px;
      border: none;
      border-bottom: 1px solid gray;
      font-size: 15px;
    }
    #submit {
      width: 150px;
      height: 30px;
      border-width: 2px;
      border-color: white;
      border-style: groove;
      border-radius: 30px;
      align-content: center;
      margin: 30px 14px 5px 80px;
      color: white;
      font-family: sans-serif;
      background-color: darkcyan;   
    }
    #avatar {
      font-size: 48px;
      color: darkcyan;
    }
    h2 {
      font-family: sans-serif;
      font-size: 20px;
      color: darkcyan;
      text-align: center;
      margin-top: 50px;
    }
    #create {
      text-align: center;
      margin-top: 20px;
    }
    a:link {
      text-decoration: none;
      color: darkcyan;
    }
    a:visited {
      text-decoration: none;
      color: darkcyan;
    }
    a:hover {
      text-decoration: underline;
      color: darkcyan;
    }
    a:active {
      text-decoration: underline;
      color: darkcyan;
    }
    #icon {
      color: darkcyan;
      margin: 20px 15px 0px 120px;
      font-size: 70px;
    }
  </style>
  <body>
  <h2> Sign in to </h2>
  <div class = "wrap">
    <form action = "/login" method="POST">
      <div>
      <i class="fa fa-user-circle" id="icon"></i>
        <input type="text"  name = "username" placeholder="Username" required>
      </div>
      <div>
        <input type="password"  name = "password" placeholder="Password" required> 
      </div>
      <div>
        <button type="submit" id = "submit"> SIGN IN </button>
      </div>
      <div id = "create"> New to ? <a href = "/signup"> Create an account </a></div>
      </form>
    </div>
  </div>
  </body>
  </html>
  `
  )
  response.end()
}