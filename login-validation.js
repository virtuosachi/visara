const db = require('./db')

module.exports = function validate (request, response) {
	const username = request.body.username;
	const password = request.body.password;
  if (username && password) {
    db.query('SELECT * FROM users WHERE username = ? AND password = ?', [username, password], 
    function(error, results) {
			if (results.length > 0) {
        response.write(`Welcome ${username}!`)       
        response.end()
			} else {
				response.write('Incorrect Username and/or Password!');
			}			
			response.end();
		});
  } 
}
