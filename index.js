const miniExpress = require('./mini-express')
const signUp = require('./signup')
const save = require('./save')
const login = require('./login-page')
const loginValidation = require('./login-validation')
const home = require('./home')
const upload = require('./upload')
const quiz = require('./quiz')
const server = miniExpress()

server.get('/', login)
server.get('/signup', signUp)
server.post('/save', save)
server.post('/login', loginValidation )
server.get('/home', home)
server.get('/upload', upload)
server.get('/quiz', quiz)


server.listen(8000, () => {
  console.log('Started server at http://localhost:8000')
})   