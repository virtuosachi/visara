module.exports = function upload(request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'})
    response.write(
    `
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>APP</title>
</head>
<style>
body {
    font-family: serif;
    background-color: white;
}

input[type=text] {
  width: 250px;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: none;
  border-bottom: 1px solid gray;
  border-radius: 1px;
  box-sizing: border-box;
  font-family: monospace;
}

.container {
    font-family: monospace;
    font-weight: bold;
    font-size: 18px;
    color: teal;
    margin-top: 30px;
    margin-left: 50px;
    padding-top: 30px;
    padding-bottom: 30px;
    padding-left: 30px;
    background-color: whitesmoke;
    width: 600px;
    
}

input[type=submit], input[type=button] {
    background-color: teal;
    color: whitesmoke;
    padding: 8px 15px;
    border: none;
    border-radius: 3px;
    cursor: pointer;
}
    
input[type=submit]:hover {
    background-color: dimgrey;
}


</style>
<body>
    <div class="container">
        <form action="/upload" enctype="multipart/form-data" method="POST">
        Album name: <input type="text" name="album" /> <br><br>
        Select files: <input type="file" name="myFiles" id="hide" />
        <input type="submit" value="Upload your files" /> <br><br><br>
        </form>
        
        <form action="/albums" method="GET">
        FIND ALBUM: <input type="text" name="find" id="find">
        <input type="submit" value="SEARCH ALBUM" id="search" />
        </form>
    </div>
    <script>
    
    window.onload = function load() {
        document.getElementById('search').onclick = function display() {
            const keyword = document.getElementById('find').value

            location.href = '/albums/' + keyword
        }
    }
    
    
    </script>

</body>
</html>
    `
    )}