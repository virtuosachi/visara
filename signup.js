module.exports = function signUp (request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'})
    response.write(
    `
    <html lang="en">
    <head>
      <title>Document</title>
    </head>
    <link rel="stylesheet" href="signUp.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
      body{
          background-image: linear-gradient(to right, #2F4F4F, white);
      }
      .signUpContainer{
          width: 60%;
          height: 600px;
          background-color:white;   
          margin: auto;
          margin-top: 50px;
          box-shadow: 0px 0px 10px grey;
      }
      #avatar {
          margin: 60px 120px 20px 100px;
          font-size: 60px;
          color: darkcyan;
      }
      h2 {
          font-family: sans-serif;
          font-size: 20px;
          color: darkcyan;
      }
      form {
          float: right;
          margin: 60px 120px 20px 100px;
          font-family: sans-serif;
      }
      input[type="text"], input[type="password"]  {
          width: 250px;
          margin-bottom: 10px;
          margin-top: 25px;
          border: none;
          border-bottom: 1px solid gray;
          font-size: 15px;
      }
      label[type="checkbox"] {
          margin-bottom: 25px;
      }
      #submit {
          width: 150px;
          height: 30px;
          border-width: 2px;
          border-color: white;
          border-style: groove;
          border-radius: 30px;
          align-content: center;
          text-align: center;
          color: white;
          font-family: sans-serif;
          background-color: darkcyan;   
      } 
      .submit{
        float: right;
        margin-top: 20px;
      }
      .colored-left{
          width: 45%;
          height: 600px;
          background-image: linear-gradient(darkcyan, #2F4F4F); 
          float: left;
      }
      .welcome{
          width: auto;
          margin: 200px 50px 20px 50px;
          color: white;
          font-family: sans-serif;
          font-size: 40px;
      }
      .button{
          width: 150px;
          height: 30px;
          margin: 50px 125px 20px 125px;
          border-width: 2px;
          border-color: white;
          border-style: groove;
          border-radius: 30px;
          align-content: center;
          text-align: center;
          color: white;
          font-family: sans-serif;
          background-color: darkcyan;   
      }
    </style>
    <body onload = "background()">
        <div class = "signUpContainer">
            <form id = "signup" action="/save" method="POST"> 
                <h2> <i class="fa fa-user-plus" style="font-size:30px;color:darkcyan"></i> Sign up Now!</h2>
                <div>
                    
                    <input type = "text" name = "firstName" placeholder="First Name" required> 
                </div>
                <div>
                    <input type = "text" name = "lastName" placeholder="Last Name" required> 
                </div>
                <div>
                    <input type = "text" name = "username" placeholder="Username" required> 
                </div>
                <div>
                    <input type = "text" name = "school" placeholder="School" required> 
                </div>
                <div>
                    <input type="checkbox" id = "teacher" value = "Teacher" onclick = "disable1()"> <label> Teacher </label>
                    <input type="checkbox" id = "student" value = "Student" onclick = "disable2()"> <label> Student </label>
                </div>
                <div>
                    <input type = "password" name = "password" placeholder="Password" required> 
                </div>
                <div class="submit">
                    <button type="submit" id = "submit" > SIGN UP </button>
                </div>
            </form>
            <div class = "colored-left">
                <div class = "welcome">
                    <b>Welcome Back!</b>
                </div>
                <a href="/">
                  <button class="button">
                      SIGN IN
                  </button>
                </a>
            </div>
        </div>
        <script>
          function disable1(){
            if (document.getElementById("teacher").checked) {
              document.getElementById("student").disabled=true
             } else {
              document.getElementById("student").disabled=false
            }
          }
          function disable2(){
            if (document.getElementById("student").checked) {
              document.getElementById("teacher").disabled=true
             } else {
              document.getElementById("teacher").disabled=false
            }
          }
        </script>
    </body>
    </html>
    `
    )
    response.end()
  }